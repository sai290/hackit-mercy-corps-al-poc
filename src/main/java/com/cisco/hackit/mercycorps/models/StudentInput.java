package com.cisco.hackit.mercycorps.models;

import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.util.Date;

public class StudentInput {

    @Id
    private String id;
    private Student student;
    private LocalDate value;

    public StudentInput(LocalDate value, Student student) {
        this.value = value;
        this.student = student;
    }

    public StudentInput() {

    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getValue() {
        return value;
    }

    public void setValue(LocalDate value) {
        this.value = value;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}