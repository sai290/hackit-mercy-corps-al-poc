package com.cisco.hackit.mercycorps.models;

import org.springframework.data.annotation.Id;

public class Condition {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Id
    private String id;
    private String name;
    private Object valueToMeet;

    public Condition(String name, Object valueToMeet) {
        this.name = name;
        this.valueToMeet = valueToMeet;
    }

    public Condition (){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValueToMeet() {
        return valueToMeet;
    }

    public void setValueToMeet(Object valueToMeet) {
        this.valueToMeet = valueToMeet;
    }
}