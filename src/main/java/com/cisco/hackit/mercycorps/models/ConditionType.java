package com.cisco.hackit.mercycorps.models;

/**
 * Created by ppatapan on 6/27/2018.
 */
public enum ConditionType {
    EQUALS,
    LESSTHAN,
    GREATERTAHN,
    LESSTHANOREQUAL,
    GREATERTAHNOREQUAL,
    CONTAINS,
    MANUAL,
    OTHER

}
