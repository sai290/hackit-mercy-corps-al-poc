package com.cisco.hackit.mercycorps.models;

import org.springframework.data.annotation.Id;

public class Student {
    @Id
    private String id;
    private Beneficiary beneficiary;
    private String program;
    private String name;
    public Student(){

    }
    public Student(String name) {
        this.name = name;
    }
    public Student(String name,String program) {
        this.name = name;
        this.program = program;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProgram() {
        return program;
    }

    public void setAidConfig(String program) {
        this.program = program;
    }
    @Override
    public boolean equals(Object s){
        if (s instanceof Student){
            return ((Student) s).getId().equals(getId());
        }else {
            return false;
        }
    }
}