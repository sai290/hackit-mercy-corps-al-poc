package com.cisco.hackit.mercycorps.models;

import javax.persistence.Id;

/**
 * Created by ppatapan on 6/27/2018.
 */
public class ConditionStatement {

    @Id
    private String id;
    private String name;
    private ConditionType inputType;
    private ConditionAggregation aggregateType;
    private String field;
    private String value;

    public ConditionStatement(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ConditionType getInputType() {
        return inputType;
    }

    public void setInputType(ConditionType inputType) {
        this.inputType = inputType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ConditionAggregation getAggregateType() {
        return aggregateType;
    }

    public void setAggregateType(ConditionAggregation aggregateType) {
        this.aggregateType = aggregateType;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
