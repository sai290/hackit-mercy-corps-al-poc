package com.cisco.hackit.mercycorps.models;

public class ReportEntry {
    private String studentName;
    private String beneficiaryName;
    private Boolean conditionMeet;
    private Boolean validated;

    public ReportEntry(String studentName, String beneficiaryName, Boolean conditionMeet, Boolean validated) {
        this.studentName=studentName;
        this.beneficiaryName=beneficiaryName;
        this.conditionMeet=conditionMeet;
        this.validated=validated;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public Boolean getConditionMeet() {
        return conditionMeet;
    }

    public void setConditionMeet(Boolean conditionMeet) {
        this.conditionMeet = conditionMeet;
    }

    public Boolean getValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }
}