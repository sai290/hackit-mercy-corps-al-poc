package com.cisco.hackit.mercycorps.models;

import java.time.LocalDate;

/**
 * Created by ppatapan on 5/11/2018.
 */
public class SearchRequest {
    String name;
    String participantType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParticipantType() {
        return participantType;
    }

    public void setParticipantType(String participantType) {
        this.participantType = participantType;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getTo() {
        return to;
    }

    public void setTo(LocalDate to) {
        this.to = to;
    }

    private LocalDate from;
    private LocalDate to;
}
