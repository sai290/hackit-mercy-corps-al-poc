package com.cisco.hackit.mercycorps.models;

/**
 * Created by ppatapan on 6/27/2018.
 */
public enum ConditionAggregation {
    SUM,
    AVERAGE,
    MAX,
    MIN,
    MEAN,
    EQUALS,
    EXTERNAL
}
