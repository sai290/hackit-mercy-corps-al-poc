package com.cisco.hackit.mercycorps.models;

public enum ProgramInputType {

    STRING,
    DEVICE,
    NUMBER,
    DATE,
    INTEGER,
    BOOL,
    IMAGE,
    FILE

}
