package com.cisco.hackit.mercycorps.models;

import org.springframework.data.annotation.Id;

import java.time.LocalDate;

public class AidTransfer {

    @Id
    private String id;

    private Beneficiary beneficiary;
    private Double amount;
    private Boolean received = Boolean.FALSE;
    private LocalDate date;
    private Student student;
    public AidTransfer(){

    }
    public AidTransfer(Beneficiary beneficiary, Boolean received, Double amount, Student student) {
        this.beneficiary = beneficiary;
        this.received = received;
        this.amount = amount;
        this.date = LocalDate.now();
        this.student = student;
    }

    public AidTransfer(Beneficiary beneficiary, Boolean received, Double amount, Student student, LocalDate date) {
        this.beneficiary = beneficiary;
        this.amount = amount;
        this.received = received;
        this.date = date;
        this.student = student;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }

    public Boolean getReceived() {
        return received;
    }

    public void setReceived(Boolean received) {
        this.received = received;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
