package com.cisco.hackit.mercycorps.models;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Document
public class Program {

    @Id
    private String id;
    private String api;
    private String category;
    private List<ProgramParameter> nclApiParameters = new ArrayList<>();
    private List<ConditionStatement> conditionStatements = new ArrayList<>();
    private Float fund;
    private String frequency;

    public Program(){

    }

    public Program(String id) {
        this.id = id;
    }

    public Program(String id, String api) {
        this.id = id;
        this.api = api;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public List<ProgramParameter> getNclApiParameters() {
        return nclApiParameters;
    }

    public void setNclApiParameters(List<ProgramParameter> nclApiParameters) {
        this.nclApiParameters = nclApiParameters;
    }

    public Float getFund() {
        return fund;
    }

    public void setFund(Float fund) {
        this.fund = fund;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public List<ConditionStatement> getConditionStatements() {
        return conditionStatements;
    }

    public void setConditionStatements(List<ConditionStatement> conditionStatements) {
        this.conditionStatements = conditionStatements;
    }

}
