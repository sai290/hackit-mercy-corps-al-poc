package com.cisco.hackit.mercycorps.models;

import java.util.ArrayList;
import java.util.List;

public class ProgramInput {

    private List<Object> value = new ArrayList<>();

    public ProgramInput(){

    }

    public ProgramInput(Object... values){
        for (Object v : values) {
            value.add(v);
        }
    }

    public List<Object> getValue() {
        return value;
    }

    public void setValue(List<Object> value) {
        this.value = value;
    }
}
