package com.cisco.hackit.mercycorps.models;

import org.springframework.data.annotation.Id;

public class BankTransferAidMechanism implements AidTransferMechanism {

    @Id
    private String id;

    private String bankName;
    private String accountNo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
}