package com.cisco.hackit.mercycorps.models;

import org.springframework.data.annotation.Id;

public class AidConfig {

    @Id
    private String id;

    private String name;
    private Double amount;

    public AidConfig(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}