package com.cisco.hackit.mercycorps.models;

import org.springframework.data.annotation.Id;

public class Beneficiary {
    @Id
    private String id;
    private String name;
    private String phone;
    private BankTransferAidMechanism aid;

    public Beneficiary() {

    }

    public Beneficiary(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public Beneficiary(String name, String phone, BankTransferAidMechanism aid) {
        this.name = name;
        this.phone = phone;
        this.aid = aid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BankTransferAidMechanism getAid() {
        return aid;
    }

    public void setAid(BankTransferAidMechanism aid) {
        this.aid = aid;
    }
}