package com.cisco.hackit.mercycorps.models;


import javax.persistence.Id;

public class ProgramParameter {

    @Id
    private String id;
    private String name;
    private ProgramInputType inputType;
    private Boolean required;

    public ProgramParameter(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProgramInputType getInputType() {
        return inputType;
    }

    public void setInputType(ProgramInputType inputType) {
        this.inputType = inputType;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }
}
