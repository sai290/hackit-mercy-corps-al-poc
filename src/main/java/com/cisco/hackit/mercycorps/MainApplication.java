package com.cisco.hackit.mercycorps;

import com.cisco.hackit.mercycorps.service.InputService;
import com.cisco.hackit.mercycorps.dao.*;
import com.cisco.hackit.mercycorps.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Month;
import java.util.Random;

@SpringBootApplication
public class MainApplication implements CommandLineRunner {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MainApplication.class, args);
    }

    @Autowired
    private BeneficiaryRepository beneficiaryRepository;

    @Autowired
    private ConditionRepository conditionRepository;

    @Autowired
    private AidConfigRepository aidConfigRepository;

    @Autowired
    private StudentInputRepository studentInputRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private AidTransferRepository aidTransferRepository;

    @Autowired
    private InputService inputService;

    @Override
    public void run(String... args){

        beneficiaryRepository.deleteAll();
        aidConfigRepository.deleteAll();
        studentInputRepository.deleteAll();
        studentRepository.deleteAll();
        aidTransferRepository.deleteAll();

        AidConfig aidConfig = aidConfigRepository.findByName("Sub-Saharan Girls School Incentive");
        if (aidConfig == null){
            aidConfig = new AidConfig("Sub-Saharan Girls School Incentive");
            aidConfig.setAmount(350.0);
            aidConfigRepository.save(aidConfig);
        }

        Condition condition = conditionRepository.findByName("Sub-Saharan Girls School Incentive");
        if (condition == null){
            condition = new Condition("Sub-Saharan Girls School Incentive", 80);
            conditionRepository.save(condition);
        }

        Beneficiary b1 = new Beneficiary("Family 1", "(123) 456-1789");
        beneficiaryRepository.save(b1);
        Student b1s1 = new Student("Jenny","Sub-Saharan Girls School Incentive");
        b1s1.setBeneficiary(b1);
        Student b1s2 = new Student("Jane", "Sub-Saharan Girls School Incentive");
        b1s2.setBeneficiary(b1);
        studentRepository.save(b1s1);
        studentRepository.save(b1s2);

        Beneficiary b2 = new Beneficiary("Family 2", "(456) 789-1012");
        beneficiaryRepository.save(b2);
        Student b2s1 = new Student("Amy","Sub-Saharan Girls School Incentive");
        b2s1.setBeneficiary(b2);
        studentRepository.save(b2s1);

        Beneficiary b3 = new Beneficiary("Family 3", "(543) 120-1987");
        beneficiaryRepository.save(b3);
        Student b3s1 = new Student("Mary", "Sub-Saharan Girls School Incentive");
        b3s1.setBeneficiary(b3);
        studentRepository.save(b3s1);
        Student b3s2 = new Student("Christine","Sub-Saharan Girls School Incentive");
        b3s2.setBeneficiary(b3);
        studentRepository.save(b3s2);
        Student b3s3 = new Student("Hannah", "Sub-Saharan Girls School Incentive");
        b3s3.setBeneficiary(b3);
        studentRepository.save(b3s3);

        Beneficiary b4 = new Beneficiary("Family 4", "(837) 483-6725");
        beneficiaryRepository.save(b4);
        Student b4s1 = new Student("Josephine", "Sub-Saharan Girls School Incentive");
        b4s1.setBeneficiary(b4);
        studentRepository.save(b4s1);

        Beneficiary b5 = new Beneficiary("Family 5", "(738) 364-8364");
        beneficiaryRepository.save(b5);
        Student b5s1 = new Student("Judith", "Sub-Saharan Girls School Incentive");
        b5s1.setBeneficiary(b5);
        studentRepository.save(b5s1);

        Beneficiary b6 = new Beneficiary("Family 6", "(563) 849-3211");
        beneficiaryRepository.save(b6);
        Student b6s1 = new Student("Karen", "Sub-Saharan Girls School Incentive");
        b6s1.setBeneficiary(b6);
        studentRepository.save(b6s1);
        Student b6s2 = new Student("Mary", "Sub-Saharan Girls School Incentive");
        b6s2.setBeneficiary(b6);
        studentRepository.save(b6s2);

        Beneficiary b7 = new Beneficiary("Family 7", "(454) 545-5454");
        beneficiaryRepository.save(b7);
        Student b7s1 = new Student("Patricia", "Sub-Saharan Girls School Incentive");
        b7s1.setBeneficiary(b7);
        studentRepository.save(b7s1);

        Beneficiary b8 = new Beneficiary("Family 8", "(543) 120-4987");
        beneficiaryRepository.save(b8);
        Student b8s1 = new Student("Caitlin", "Sub-Saharan Girls School Incentive");
        b8s1.setBeneficiary(b8);
        studentRepository.save(b8s1);
        Student b8s2 = new Student("Brooklyn", "Sub-Saharan Girls School Incentive");
        b8s2.setBeneficiary(b8);
        studentRepository.save(b8s2);
        Student b8s3 = new Student("Susan", "Sub-Saharan Girls School Incentive");
        b8s3.setBeneficiary(b8);
        studentRepository.save(b8s3);
        Student b8s4 = new Student("Evelyn", "Sub-Saharan Girls School Incentive");
        b8s4.setBeneficiary(b8);
        studentRepository.save(b8s4);

        Beneficiary b9 = new Beneficiary("Family 9", "(183) 928-3913");
        beneficiaryRepository.save(b9);
        Student b9s1 = new Student("Julian", "Sub-Saharan Girls School Incentive");
        b9s1.setBeneficiary(b9);
        studentRepository.save(b9s1);

        for (int month = 1; month <= 4; month++) {
            for (int day = 1; day <= 30; day++) {
                try {
                    LocalDate d = LocalDate.of(2018, Month.of(month), day);
                    if (d.getDayOfWeek().getValue() <= 5) { // only create dummy data for weekdays
                        for (Student student : studentRepository.findAll()) {

                            int r = randInt(1, 10);
                            if (r != 5) {
                                StudentInput si = new StudentInput(d, student);
                                inputService.input(si, d);
                            }
                        }
                    }
                }catch (DateTimeException ex){
                    // eat it!
                }
            }
        }
        System.out.println();
    }

    private int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    @Bean
    public Condition condition(){
        return conditionRepository.findByName("Sub-Saharan Girls School Incentive");
    }

}
