package com.cisco.hackit.mercycorps.service;

import com.cisco.hackit.mercycorps.dao.AidConfigRepository;
import com.cisco.hackit.mercycorps.dao.AidTransferRepository;
import com.cisco.hackit.mercycorps.dao.StudentInputRepository;
import com.cisco.hackit.mercycorps.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class InputService {

    private Condition condition;
    private AidConfigRepository aidConfigRepository;
    private StudentInputRepository studentInputRepository;
    private AidTransferRepository aidTransferRepository;

    @Autowired
    public InputService(Condition condition, AidConfigRepository aidConfigRepository, StudentInputRepository studentInputRepository, AidTransferRepository aidTransferRepository) {
        this.condition = condition;
        this.studentInputRepository = studentInputRepository;
        this.aidConfigRepository = aidConfigRepository;
        this.aidTransferRepository = aidTransferRepository;
    }

    public void input(StudentInput studentInput, LocalDate date){
        // date is just a hack to accommodate the dummy data.
        // pass null for date on a normal basis
        Student student = studentInput.getStudent();
        studentInputRepository.save(studentInput);
        Integer valueToMeet = (Integer) condition.getValueToMeet();
        LocalDate initial = LocalDate.now();
        LocalDate start = initial.withDayOfMonth(1);
        LocalDate end = initial.withDayOfMonth(initial.lengthOfMonth());
        if (date != null) {
            start = date.withDayOfMonth(1);
            end = date.withDayOfMonth(initial.lengthOfMonth());
        }
        List<StudentInput> inputs = studentInputRepository.findByValueBetweenAndStudent(start, end, student);
        float attendance = inputs.size();
        float b = (attendance/20);
        float pct = b * 100;
        if (pct >= valueToMeet){
            distriuteAid(student, date);
        }
    }

    private void distriuteAid(Student student, LocalDate date){
        AidConfig aidConfig = aidConfigRepository.findByName("Sub-Saharan Girls School Incentive");
        Beneficiary beneficiary = student.getBeneficiary();

        // Make sure we aren't paying twice for a student in 1 month
        LocalDate initial = LocalDate.now();
        LocalDate start = initial.withDayOfMonth(1);
        LocalDate end = initial.withDayOfMonth(initial.lengthOfMonth());
        List<AidTransfer> xfrs = aidTransferRepository.findByBeneficiaryAndDateBetween(beneficiary,start, end);

        boolean sentThisMonthAlready = false;
        for (AidTransfer xfr : xfrs) {
            if (xfr.getStudent().equals(student)){
                sentThisMonthAlready = true;
            }
        }

        if (!sentThisMonthAlready) {
            BankTransferAidMechanism aidMechanism = beneficiary.getAid();
            AidTransfer aidTransfer = date == null
                    ? new AidTransfer(beneficiary, true, aidConfig.getAmount(), student)
                    : new AidTransfer(beneficiary, true, aidConfig.getAmount(), student, date);
            aidTransferRepository.save(aidTransfer);
        }
    }

}