package com.cisco.hackit.mercycorps.controller;

import com.cisco.hackit.mercycorps.dao.AidConfigRepository;
import com.cisco.hackit.mercycorps.dao.AidRepository;
import com.cisco.hackit.mercycorps.dao.BeneficiaryRepository;
import com.cisco.hackit.mercycorps.models.Aid;
import com.cisco.hackit.mercycorps.models.AidConfig;
import com.cisco.hackit.mercycorps.models.Beneficiary;
import com.cisco.hackit.mercycorps.models.Condition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "api/beneficiary")
public class BeneficiaryController {
    @Autowired
    private BeneficiaryRepository beneficiaryRepository;
    @Autowired
    private AidRepository aidRepository;
    @Autowired
    private AidConfigRepository aidConfigRepository;

    @RequestMapping(value = "reqigsterbeneciary", method = RequestMethod.POST)
    public Boolean register(@RequestBody Beneficiary beneficiary) throws Exception {
        Boolean result=false;
        AidConfig aidConfig = aidConfigRepository.findByName("Sub-Saharan Girls School Incentive");
        Double amount=aidConfig.getAmount();
        Aid aid=new Aid();
        aid.setAmount(amount);
        aid.setBeneficiary(beneficiary);
        aid.setReceived(false);
        beneficiaryRepository.save(beneficiary);
        aidRepository.save(aid);
        result=true;
        return result;
    }

}