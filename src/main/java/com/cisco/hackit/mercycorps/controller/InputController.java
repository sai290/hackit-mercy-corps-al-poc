package com.cisco.hackit.mercycorps.controller;

import com.cisco.hackit.mercycorps.models.StudentInput;
import com.cisco.hackit.mercycorps.service.InputService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InputController {

    @Autowired
    InputService inputService;

    @RequestMapping(value="inputStudentAttendance", method = RequestMethod.POST)
    public void inputStudentAttendance(@RequestBody StudentInput studentInput){
        inputService.input(studentInput, null);
    }

}
