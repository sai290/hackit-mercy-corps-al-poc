package com.cisco.hackit.mercycorps.controller;

import com.cisco.hackit.mercycorps.dao.AidTransferRepository;
import com.cisco.hackit.mercycorps.dao.ConditionRepository;
import com.cisco.hackit.mercycorps.dao.StudentInputRepository;
import com.cisco.hackit.mercycorps.dao.StudentRepository;
import com.cisco.hackit.mercycorps.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "api/mercyCorps")
public class MercyCorpsController {

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private StudentInputRepository studentInputRepository;
    @Autowired
    private ConditionRepository conditionRepository;
    @Autowired
    AidTransferRepository aidTransferRepository;

    @RequestMapping(value = "report", method = RequestMethod.POST)
    @CrossOrigin
    public List<ReportEntry> view(@RequestBody ReportReq req) throws Exception {
        return this.generateReport(req.program, req.dateInput);
    }

    private List<ReportEntry> generateReport(String program,Integer dateInput) {
        Condition condition=conditionRepository.findByName(program);
        Integer valueToMeet = (Integer) condition.getValueToMeet();
        List<ReportEntry> reports=new ArrayList<>();
        LocalDate start = LocalDate.now().withMonth(dateInput).withDayOfMonth(1);
        LocalDate end = start.withDayOfMonth(start.lengthOfMonth());
        List<Student> students=studentRepository.findAll();
        for(Student student: students) {
            Boolean conditionMeet=false;
            List<StudentInput> inputs = studentInputRepository.findByValueBetweenAndStudent(start, end, student);
            float attendance = inputs.size();
            float b = (attendance/20);
            float pct = b * 100;
            if (pct >= valueToMeet){
                conditionMeet=true;
            }
            ReportEntry entry=new ReportEntry(student.getName(), student.getBeneficiary().getName(), conditionMeet, true);
            reports.add(entry);
        };
       return reports;
    }

    @RequestMapping(path = "/transactions",method = RequestMethod.GET)
    @CrossOrigin
    public List<AidTransfer> getTransactions() {
        List<AidTransfer> result= aidTransferRepository.findAllByOrderByDateDesc();
        return result;
    }


    @CrossOrigin
    @RequestMapping(path = "/search",method = RequestMethod.POST)
    public List<AidTransfer> searchTransactions(@RequestBody SearchRequest searchRequest) {
        List<AidTransfer> result= new ArrayList<>();
//        Beneficiary beneficiary = new Beneficiary();
//        beneficiary.setName(searchRequest.getName());
//        result = aidTransferRepository.findByBeneficiaryAndDateBetween(beneficiary,searchRequest.getFrom(),searchRequest.getTo());
        if(searchRequest.getParticipantType().equalsIgnoreCase("beneficiary")) {
            Beneficiary beneficiary = new Beneficiary();
            beneficiary.setName(searchRequest.getName());
            result = aidTransferRepository.findByBeneficiaryAndDateBetween(beneficiary,searchRequest.getFrom(),searchRequest.getTo());
        }
        else{
            Student student = new Student();
            student.setName(searchRequest.getName());
            result = aidTransferRepository.findByStudentAndDateBetween(student,searchRequest.getFrom(),searchRequest.getTo());
        }
        return result;
    }

    public static class ReportReq{
        public String program;
        public Integer dateInput;
    }
}