package com.cisco.hackit.mercycorps.dao;

import com.cisco.hackit.mercycorps.models.Beneficiary;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(collectionResourceRel = "beneficiaries", path = "beneficiaries")
@CrossOrigin
public interface BeneficiaryRepository extends MongoRepository<Beneficiary, String> {
    Beneficiary findByName(String name);

}