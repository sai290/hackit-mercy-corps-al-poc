package com.cisco.hackit.mercycorps.dao;

import com.cisco.hackit.mercycorps.models.Student;
import com.cisco.hackit.mercycorps.models.StudentInput;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "studentinputs", path = "studentinputs")
@CrossOrigin
public interface StudentInputRepository extends MongoRepository<StudentInput, String> {

    List<StudentInput> findByValueBetweenAndStudent(LocalDate start, LocalDate end, Student student);
    List<StudentInput> findByValueBetweenAndStudent_Id(LocalDate start, LocalDate end, String studentId);

}