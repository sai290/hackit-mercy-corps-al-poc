package com.cisco.hackit.mercycorps.dao;

import com.cisco.hackit.mercycorps.models.AidTransfer;
import com.cisco.hackit.mercycorps.models.Beneficiary;
import com.cisco.hackit.mercycorps.models.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "aidtransfer", path = "aidtransfer")
@CrossOrigin
public interface AidTransferRepository extends MongoRepository<AidTransfer, String> {

    List<AidTransfer> findByBeneficiary(Beneficiary beneficiary);
    List<AidTransfer> findByBeneficiaryAndDateBetween(Beneficiary beneficiary, LocalDate start, LocalDate end);
//    List<AidTransfer> findByBeneficary_Name(@Param("name") String name);
    //Optional<AidTransfer> findById(String id);
    List<AidTransfer> findAllByOrderByDateDesc();
    List<AidTransfer> findByStudentAndDateBetween(Student student, LocalDate start, LocalDate end);
    List<AidTransfer> findAllByStudent(Student student);

}
