package com.cisco.hackit.mercycorps.dao;

import com.cisco.hackit.mercycorps.models.AidConfig;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(collectionResourceRel = "aidconfigure", path = "aidconfigure")
@CrossOrigin
public interface AidConfigRepository extends MongoRepository<AidConfig, String>{
    AidConfig findByName(String name);
}