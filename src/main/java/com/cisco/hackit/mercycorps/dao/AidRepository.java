package com.cisco.hackit.mercycorps.dao;

import com.cisco.hackit.mercycorps.models.Aid;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(collectionResourceRel = "aids", path = "aids")
@CrossOrigin
public interface AidRepository extends MongoRepository<Aid, String> {

}