package com.cisco.hackit.mercycorps.dao;

import com.cisco.hackit.mercycorps.models.BankTransferAidMechanism;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "bankAidTransferMechanism,", path = "bankAidTransferMechanism,")
public interface BankTransferAidMechanismRepository extends MongoRepository<BankTransferAidMechanism, String> {
}