package com.cisco.hackit.mercycorps.dao;

import com.cisco.hackit.mercycorps.models.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(collectionResourceRel = "student", path = "student")
@CrossOrigin
public interface StudentRepository extends MongoRepository<Student, String> {
    Student findByName(String name);
}