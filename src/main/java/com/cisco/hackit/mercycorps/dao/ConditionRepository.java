package com.cisco.hackit.mercycorps.dao;

import com.cisco.hackit.mercycorps.models.Condition;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(collectionResourceRel = "conditions", path = "conditions")
@CrossOrigin
public interface ConditionRepository extends MongoRepository<Condition, String> {

    Condition findByName(String name);

}