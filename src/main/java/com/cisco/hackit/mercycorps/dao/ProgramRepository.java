package com.cisco.hackit.mercycorps.dao;


import com.cisco.hackit.mercycorps.models.Program;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(collectionResourceRel = "program", path = "program")
@CrossOrigin
public interface ProgramRepository extends MongoRepository<Program, String> {

//    Program findByApi(@Param("api") String api);
//    Program findById(@Param("api") Id api);
    //Optional<Program> findById(@Param("api") String id);
}
