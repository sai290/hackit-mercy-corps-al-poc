package com.cisco.hackit.mercycorps;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

//@Configuration
public class MongoConfiguration extends AbstractMongoConfiguration {

    @Autowired
    Environment environment;

    @Override
    protected String getDatabaseName() {
        return environment.getProperty("mongo.database-name");
    }

//    @Override
//    public Mongo mongo() throws Exception {
////        System.out.printf("\n\n=============\n\n\nenv: %s:%s\n\n=============\n\n",
////                environment.getProperty("MongoServer"),
////                environment.getProperty("MongoPort")
////        );
//        try {
//            return new MongoClient(new MongoClientURI(environment.getProperty("mongo.connectionstring")));
//        }
//        catch (Exception ex) {
//            System.out.println(ex);
//            ex.printStackTrace();
//            return new MongoClient("192.168.99.100", 27017);
//        }
//    }

    @Autowired
    private MongoDbFactory mongoFactory;

    @Autowired
    private MongoMappingContext mongoMappingContext;

    @Bean
    public MappingMongoConverter mongoConverter() throws Exception {
        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoFactory);
        MappingMongoConverter mongoConverter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
        //this is my customization
        mongoConverter.setMapKeyDotReplacement("_");
        mongoConverter.afterPropertiesSet();
        return mongoConverter;
    }

    @Override
    public MongoClient mongoClient() {
        try {
            return new MongoClient(new MongoClientURI(environment.getProperty("mongo.connectionstring")));
        }
        catch (Exception ex) {
            System.out.println(ex);
            ex.printStackTrace();
            return new MongoClient("192.168.99.100", 27017);
        }
    }
}
